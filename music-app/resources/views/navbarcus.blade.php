<ul class="navbar-nav float-end">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
        aria-expanded="false">
        <span class="d-none d-md-block">{{ Auth::user()->username }} </span>
        <span class="d-block d-md-none"><i class="fa fa-plus"></i></span>
    </a>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="#"
            id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="../assets/images/users/1.jpg" alt="user" class="rounded-circle" width="31" />
        </a>
        <ul class="dropdown-menu dropdown-menu-end user-dd animated" aria-labelledby="navbarDropdown">
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('logout') }}"><i class="fa fa-power-off me-1 ms-1"></i> Logout</a>
            <div class="dropdown-divider"></div>
            <div class="ps-4 p-10">
                <a href="{{ route('user.edit', Auth::user()->id) }}"
                    class="btn btn-sm btn-success btn-rounded text-white">View Profile</a>
            </div>
        </ul>
    </li>
</ul>
