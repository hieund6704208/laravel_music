<?php

use App\Http\Controllers\AlbumController;
use App\Http\Controllers\Customer\CoinController;
use App\Http\Controllers\Customer\CustomerController;
use App\Http\Controllers\Customer\DownloadController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\SingerController;
use App\Http\Controllers\SongController;
use App\Http\Controllers\Customer\SongController as SonggController;
use App\Http\Controllers\FavoriteController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\CheckRole;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'music' ], function()
{
    Route::get('login',[LoginController::class,'showLoginForm'])->name('login');
    Route::post('login',[LoginController::class,'login']);
    Route::get('register',[LoginController::class,'showRegistrationForm'])->name('register');
    Route::post('register',[LoginController::class,'register']);
    Route::get('logout',[LoginController::class,'logout'])->name('logout');
});

Route::group(['middleware' => 'checkrole'],function () {
    Route::resource('/genre',GenreController::class);
    Route::resource('/singer',SingerController::class);
    Route::resource('/album',AlbumController::class);
    Route::resource('/song',SongController::class);
});

Route::post('/favorite/add/{song}',[ FavoriteController::class,'addFavorite'])->name('add');
Route::delete('/favorite/del/{song}',[ FavoriteController::class,'removeFavorite'])->name('del');
Route::get('/favorite/index',[ FavoriteController::class,'index'])->name('index');
Route::resource('/customer',SonggController::class);
Route::get('/home',[CustomerController::class, 'index'])->name('home');
Route::resource('/customer/coin',CoinController::class);
Route::resource('/user',UserController::class);
Route::resource('/coin',CoinController::class);
Route::post('/download', [DownloadController::class,'download'])->name('download');
Route::post('/user/addcoin/{id}', [UserController::class,'deposit'])->name('deposit');

