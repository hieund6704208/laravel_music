// Truy cập vào các thành phần
const musicContainer = document.getElementById("music-container");
const playBtn = document.getElementById("play");
const prevBtn = document.getElementById("prev");
const nextBtn = document.getElementById("next");

const audio = document.getElementById("audio");
const progress = document.getElementById("progress");
const progressContainer = document.getElementById("progress-container");
const title = document.getElementById("title");
const cover = document.getElementById("cover");

var songIndex = 2;
function playMusic(file,key,title, image) {
    var musicValue = file.replace('.mp3', '');
    if(key || key == 0){
        var sindex = JSON.parse(file);
        var songs = sindex[key];
        loadSong(songs,title, image);
    }
    else{
        var songs = JSON.parse(file);
        loadSong(songs[songIndex],title, image);
    }
    // loadSong(songs[songIndex]);
    playSong();

}

// Cập nhật thông tin bài hát
function loadSong(song,t,i) {
    title.innerText = t;
    // console.log(song);
    audio.src = `singers/${song}`;
    cover.src = `singers/${i}`;
}

// Play song
function playSong(def = false) {
    const defaultMusic = '1678766343.mp3';
    const defaultTitle = 'Hen';
    musicContainer.classList.add("play");
    playBtn.querySelector("i.fas").classList.remove("fa-play");
    playBtn.querySelector("i.fas").classList.add("fa-pause");
    if(def){

        loadSong(defaultMusic,defaultTitle);
    }

    audio.play();
}

// Pause song
function pauseSong() {
    musicContainer.classList.remove("play");
    playBtn.querySelector("i.fas").classList.add("fa-play");
    playBtn.querySelector("i.fas").classList.remove("fa-pause");

    audio.pause();
}

// Xử lý khi prev bài hát
function prevSong(file, title) {

    // Xử lý khi prev bài hát
    var songs = JSON.parse(file);
    var titles = JSON.parse(title);
    // console.log(titles);
    songIndex--;

    // Nếu đang là bài hát đầu thì quay lại bài hát cuối
    if (songIndex < 0) {
        songIndex = songs.length - 1;
    }

    // Cập nhật thông tin của bài hát lên giao diện
    loadSong(songs[songIndex],titles[songIndex]);

    // Phát nhạc
    playSong();
}

// Next song
function nextSong(file, title) {
    // console.log(title);
    var songs = JSON.parse(file);
    var titles = JSON.parse(title);
    // Tăng index của songIndex lên 1
    songIndex++;
    // Nếu đang là bài hát cuối thì quay lại bài hát đầu
    if (songIndex > songs.length - 1) {
        songIndex = 0;
    }

    // Cập nhật thông tin của bài hát lên giao diện
    loadSong(songs[songIndex],titles[songIndex]);

    // Phát nhạc
    playSong();
}

// Update progress bar
function updateProgress(e) {
    const { duration, currentTime } = e.srcElement;
    const progressPercent = (currentTime / duration) * 100;
    progress.style.width = `${progressPercent}%`;
}

// Set progress bar
function setProgress(e) {
    const width = this.clientWidth;
    const clickX = e.offsetX;
    const duration = audio.duration;

    audio.currentTime = (clickX / width) * duration;
}

// Lắng nghe sự kiện
playBtn.addEventListener("click", () => {
    // Kiểm tra xem musicContainer có chứa class "play" hay không?
    const isPlaying = musicContainer.classList.contains("play");

    // Nếu có thì thực hiện pause
    // Nếu không thì thực hiện play
    if (isPlaying) {
        pauseSong();
        console.log(1111);
    } else {
        console.log(222);
        playSong(false);
        //dừng bài, tiếp tục bài
    }
});

// Lắng nghe sự kiện khi next và prev bài hát
prevBtn.addEventListener("click", prevSong);
nextBtn.addEventListener("click", nextSong);

// Time/song update
audio.addEventListener("timeupdate", updateProgress);

// Click on progress bar
progressContainer.addEventListener("click", setProgress);

// Lắng nghe sự kiện khi kết thúc bài hát
audio.addEventListener("ended", nextSong);

