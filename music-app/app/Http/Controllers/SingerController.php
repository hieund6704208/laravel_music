<?php

namespace App\Http\Controllers;

use App\Http\Requests\SingerRequest;
use App\Models\Singer;
use App\Services\SingerService;
use Exception;
use Illuminate\Http\Request;

class SingerController extends Controller
{
    protected $singerService;

    public function __construct(SingerService $singerService)
    {
        $this->middleware('auth:web', ['except' => 'login']);
        $this->singerService = $singerService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $singers = $this->singerService->getAll();
            return view('singers.index', compact('singers'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('singers.create');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SingerRequest $request)
    {
        try {
            $data = $request->all();
            $singer = $this->singerService->create($data);
            return redirect()->route('singer.store')->with('success', 'Singer created successfully.');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $singer = $this->singerService->getById($id);
            return view('singers.edit', compact('singer'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SingerRequest $request, $id)
    {
        try {
            $data = $request->all();
            $singer = $this->singerService->update($id, $data);
            return redirect()->route('singer.index')->with('success', 'Singer updated successfully.');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->singerService->delete($id);
            return redirect()->route('singer.index')->with('success', 'Singer deleted successfully.');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }
}
