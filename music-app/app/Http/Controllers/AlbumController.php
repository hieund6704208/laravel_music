<?php

namespace App\Http\Controllers;

use App\Http\Requests\AlbumRequest;
use App\Services\AlbumService;
use App\Services\GenreService;
use App\Services\SingerService;
use Exception;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    protected $albumService;
    protected $genreService;
    protected $singerService;

    public function __construct(AlbumService $albumService, GenreService $genreService, SingerService $singerService)
    {
        $this->middleware('auth:web', ['except' => 'login']);
        $this->albumService = $albumService;
        $this->genreService = $genreService;
        $this->singerService = $singerService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = $this->albumService->getAllAlbums();
        return view('albums.index', compact('albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $genres = $this->genreService->getAllGenres();
            $singers = $this->singerService->getAll();
            return view('albums.create', compact('genres', 'singers'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlbumRequest $request)
    {
        try {
            $this->albumService->createAlbum($request->all());
            return redirect()->route('album.index')->with('success', 'Album created successfully');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $genres = $this->genreService->getAllGenres();
            $singers = $this->singerService->getAll();
            $album = $this->albumService->getAlbumById($id);
            return view('albums.edit', compact('album', 'genres', 'singers'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AlbumRequest $request, $id)
    {
        try {
            $data = $request->all();
            $album = $this->albumService->updateAlbum($id, $data);
            return redirect()->route('album.index')->with('success', 'Singer updated successfully.');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($albumId)
    {
        try {
            $this->albumService->deleteAlbum($albumId);
            return redirect()->route('album.index')->with('success', 'Album deleted successfully.');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }
}
