<?php

namespace App\Http\Controllers;

use App\Http\Requests\SongRequest;
use App\Services\AlbumService;
use App\Services\GenreService;
use App\Services\SingerService;
use App\Services\SongService;
use Exception;
use Illuminate\Http\Request;

class SongController extends Controller
{
    protected $songService;
    protected $albumService;
    protected $genreService;
    protected $singerService;

    public function __construct(SongService $songService, AlbumService $albumService, GenreService $genreService, SingerService $singerService)
    {
        $this->middleware('auth:web', ['except' => 'login']);
        $this->songService = $songService;
        $this->albumService = $albumService;
        $this->genreService = $genreService;
        $this->singerService = $singerService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $songs = $this->songService->getAllSongs();
            return view('songs.index', compact('songs'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $songs = $this->songService->getAllSongs();
            $genres = $this->genreService->getAllGenres();
            $singers = $this->singerService->getAll();
            $albums = $this->albumService->getAllAlbums();
            return view('songs.create', compact('songs', 'genres', 'singers', 'albums'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SongRequest $request)
    {
        try {
            $songData = $request->all();
            $song = $this->songService->createSong($songData);
            return redirect()->route('song.store')->with('success', 'Singer created successfully.');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($songId)
    {
        try {
            $albums = $this->albumService->getAllAlbums();
            $song = $this->songService->getSongById($songId);
            return view('songs.edit', compact('song', 'albums'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SongRequest $request, $id)
    {
        try {
            $data = $request->all();
            $song = $this->songService->updateSong($id, $data);
            return redirect()->route('song.index')->with('success', 'Singer updated successfully.');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($songId)
    {
        try {
            $this->songService->deleteSong($songId);
            return redirect()->route('song.index')->with('success', 'Singer deleted successfully.');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }
}
