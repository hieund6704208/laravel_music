<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Mail\SendMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('login');
    }
    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        if (Auth::attempt($credentials)) {
            if (Auth::user()->role == 1) {
                return redirect()->route('genre.index');
            } else {
                return redirect()->route('customer.index');
            }
        }
        return back()->withErrors(['email' => 'Email or password is incorrect.']);
    }

    public function showRegistrationForm()
    {
        return view('register');
    }

    public function register(LoginRequest $request)
    {
        $user = User::create([
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);
        Auth::login($user);
        return redirect()->route('login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/home');
    }
    
}
