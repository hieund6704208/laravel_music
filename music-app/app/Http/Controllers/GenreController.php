<?php

namespace App\Http\Controllers;

use App\Http\Requests\GenreRequest;
use App\Models\Genre;
use App\Services\GenreService;
use Exception;
// use App\Repositories\GenreRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GenreController extends Controller
{
    protected $genreService;

    public function __construct(GenreService $genreService)
    {
        $this->middleware('auth:web', ['except' => 'login']);
        $this->genreService = $genreService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $genres = $this->genreService->getAllGenres();
            return view('genres.genre', compact('genres'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('genres.create');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenreRequest $request)
    {
        try {
            $genreData = $request->validated();
            $genre = $this->genreService->createGenre($genreData);
            return redirect()->route('genre.index')->with('success', 'Category created successfully.');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($genreId)
    {
        try {
            $category = $this->genreService->getGenreById($genreId);
            return view('genres.update', compact('category'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GenreRequest $request, $genreId)
    {
        try {
            $categoryData = $request->validated();
            $category = $this->genreService->updateGenre($genreId, $categoryData);
            return redirect()->route('genre.index')->with('success', 'Category updated successfully.');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($genreId)
    {
        try {
            $this->genreService->deleteGenre($genreId);
            return redirect()->route('genre.index')->with('success', 'Category deleted successfully.');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }
}
