<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Services\Customer\DownloadService;
use Illuminate\Http\Request;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class DownloadController extends Controller
{
    protected $downloadService;
    protected $userService;

    public function __construct(DownloadService $downloadService, UserService $userService)
    {
        $this->downloadService = $downloadService;
        $this->userService = $userService;
    }

    public function download(Request $request)
    {
        if (Auth::check()) {
            $user = $this->userService->getUserId(Auth::id());
            $total = $user->price;
            $data = $request->price;
            $newPrice = $total - $data;
            if ($newPrice < 0) {
                return redirect(route('customer.index'))->with('error', 'Vui lòng nạp thêm tiền');
            }
            $user->price = $newPrice;
            $user->save();
            return  response()->download(public_path("singers/" . $request->download));
        } else {
            return view('login');
        }
        // $user = $this->userService->getUserId(Auth::id());
        // $total = $user->price;
        // $data = $request->price;
        // $newPrice = $total - $data;
        // if ($newPrice < 0) {
        //     return redirect(route('customer.index'))->with('error', 'Vui lòng nạp thêm tiền');
        // }
        // $user->price = $newPrice;
        // $user->save();
        // return  response()->download(public_path("singers/" . $request->download));
    }
}
