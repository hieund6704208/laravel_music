<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Services\Customer\SongService;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    protected $songService;
    public function __construct(SongService $songService)
    {
        $this->songService = $songService;
    }

    public function index()
    {
        $songs = $this->songService->getAllSongs();
        $music = [];
        foreach ($songs as $song) {
            array_push($music, $song->file);
        }
        $title = [];
        foreach ($songs as $song) {
            array_push($title, $song->name);
        }
        return view('music', ['music' => $music, 'songs' => $songs, 'title' => $title]);
    }
}
