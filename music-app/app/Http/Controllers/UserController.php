<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\SongService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userService;
    protected $songService;
    public function __construct(UserService $userService, SongService $songService)
    {
        $this->userService = $userService;
        $this->songService = $songService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($userId)
    {
        if (Auth::user()->id == $userId) {
            $user = $this->userService->getUserId($userId);
            return view('users.update', compact('user'));
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'username' => 'required'
        ]);
        $data = $request->all();
        $song = $this->userService->updateUser($id, $data);
        if (Auth::user()->role == 1) {
            return redirect()->route('song.index')->with('success', 'Singer updated successfully.');
        } else {
            return redirect()->route('customer.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deposit(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $priceOld = $user->price;
        $amount = $request->input('price');
        $priceNew = $priceOld + $amount;
        $this->userService->updateCoin($id, $priceNew);
        return redirect()->route('customer.index')->with('success', 'Nạp tiền thành công!');
    }

}
