<?php

namespace App\Http\Controllers;

use App\Models\Favorite;
use App\Models\Song;
use App\Models\User;
use App\Services\Customer\SongService;
use App\Services\FavoriteService;
use App\Services\SongService as ServicesSongService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    protected $favoriteService;
    protected $songService;

    public function __construct(FavoriteService $favoriteService, SongService $songService)
    {
        $this->favoriteService = $favoriteService;
        $this->songService = $songService;
    }

    public function index()
    {

        try {
            $songs = $this->songService->getAllSongs();
                $music =[];
                foreach($songs as $song){
                    array_push($music,$song->file);
                }
                $title =[];
                foreach($songs as $song){
                    array_push($title,$song->name);
                }
            if (Auth::check()) {
                $favorites = $this->favoriteService->getFavorites();
                return view('favorite.index', compact('favorites','songs'));
            } else {
                return redirect()->route('login');
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    public function addFavorite(Request $request, string $song)
    {
        $user = User::find(Auth::id())->song()->attach($song);
        return redirect(route('customer.index'));
    }

    public function removeFavorite(Request $request, $id)
    {
        $user = Favorite::find($id)->delete();
        return redirect()->route('index');
    }
}
