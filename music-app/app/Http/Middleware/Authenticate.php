<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        // $credentials = $request->only('username', 'password');

        // if (Auth::attempt($credentials)) {
        //    if( Auth::user()->role ==1 ){
        //        return redirect()->route('genre.index');

        //    }

        //     return view('welcome');
        // }
        if (! $request->expectsJson()) {
            return route('login');
        }

    }

}
