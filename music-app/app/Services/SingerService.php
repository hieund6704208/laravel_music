<?php

namespace App\Services;

use App\Models\Singer;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class SingerService
{
    public function getAll()
    {
        return Singer::paginate(INDEX_SINGERS);
    }

    public function getById($id)
    {
        return Singer::find($id);
    }

    public function create($data)
    {
        $name = $data['name'];
        if ($data['image']) {
            $image = $data['image'];
            $fileName = time() . '.' . $image->extension();
            $image->move(public_path('singers'), $fileName);
            $singer = new Singer();
            $singer->name = $name;
            $singer->image = $fileName;
            $singer->save();
        }
        return $singer;
    }

    public function update($id, $data)
    {
        $singer = Singer::find($id);
        $name = $data['name'];
        if ($data['image']) {
            $image = $data['image'];
            $fileName = time() . '.' . $image->extension();
            $image->move(public_path('singers'), $fileName);
            File::delete(public_path('singers/' . $singer->image));
        }
        $singer = Singer::find($id);
        $singer->name = $name;
        $singer->image = $fileName;
        $singer->update($data);
    }

    public function delete($id)
    {
        $singer = Singer::find($id);
        if ($singer->image) {
            File::delete(public_path('singers/' . $singer->image));
        }
        $singer->delete();
        return true;
    }
}
