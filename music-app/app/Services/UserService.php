<?php

namespace App\Services;

use App\Models\User;

    class UserService{
        public function getUserId($userId)
        {
            return User::findOrFail($userId);
        }

        public function updateUser($userId, $userData)
        {
            $user = User::findOrFail($userId);
            $user->update($userData);
            return $user;
        }

        public function updateCoin($userId, $coinData){
            $user = User::findOrFail($userId);
            $user->update([
                'price'=>$coinData
            ]);
            return $user;
        }

    }
