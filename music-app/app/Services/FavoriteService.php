<?php
namespace App\Services;

use App\Models\Favorite;
use App\Models\Song;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class FavoriteService
{
    public function getFavorites()
    {
        return Favorite::paginate(INDEX_GENRES);
    }

    public function addFavorite(User $user, Song $song)
    {
        // dd($songId);
        // $userFavorite = User::find(Auth::user()->id);
        // dd($userFavorite);
        // $userFavorite->songs()->attach($song->id);
    }

    public function removeFavorite(User $user, Song $song)
    {
        // $userFavorite = User::find(Auth::user()->id);
        // $userFavorite->song()->detach($song->id);
    }
}
