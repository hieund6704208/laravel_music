<?php

namespace App\Services\Customer;
use Illuminate\Support\Facades\File;


use App\Models\User;

    class CoinService{

        public function getUserId($userId)
        {
            return User::findOrFail($userId);
        }

        public function createCoin($coinData)
        {

            return User::create($coinData);
        }

        public function updateCoin($userId, $coinData)
        {
            $user = User::findOrFail($userId);
            $user->update($coinData);
            return $user;
        }

    }
