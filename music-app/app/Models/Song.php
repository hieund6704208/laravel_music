<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    use HasFactory;

    protected $fillable =[ 'name', 'album_id', 'file', 'price' ];

    public function album()
    {
        // return $this->hasMany(Album::class,'album_id', 'id');
        return $this->belongsTo(Album::class,'album_id', 'id');
    }

    public function user(){
        return $this->belongsToMany(User::class,'favorites','song_id','user_id');
    }
}
